# FMI Scraper

This is a scraper for my faculty website in order to have an offline copy in case the site is unavailable.
It is a hobby project and started as a bash script one-liner. There aren't many commits yet because for a long time the script had less than 100 lines of code and it seemed overkill to version it.

After the website upgrade in mid 2020, relevant documents are since then uploaded to Google Drive.
Therefore, a brute force approach is time consuming for downloading files and completely infeasible for folders, since they would need to be archived to avoid more requests.  

The scraper currently uses the Google Drive API to download files and folders.

### Features
* [x] Download Google Drive files and folders
* [x] Export Google Drive documents, presentations and spreadsheets
* [x] Export published spreadsheets (without official API support)
* [x] Handle various URL patterns and tackle inconsistencies
* [x] Follow only relevant redirects
* [x] Basic caching: avoid downloading the same resource twice where possible

### To do
* [ ] More advanced caching system
* [ ] Use loggers
* [ ] Add timestamps to Google Drive files and/or determine if the file changed remotely
* [ ] Remove outdated comments and unused debug statements

Downloading a folder as an archive is not yet supported by the underlying gem.
It might be possible to implement this using the low-level client.
However, this approach will always produce duplicate data for nested folders referenced in multiple locations.
