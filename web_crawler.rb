# frozen_string_literal: true

require 'fileutils'

require 'google_drive'
require 'nokogiri'

require 'pry'
require 'pry-byebug'

require 'active_support/all'

# Helper methods for the Google Drive downloader class
module GoogleDriveHelper
  SUPPORTED_EXTENSIONS = %w[.docx .xlsx .pptx].freeze

  private

  def download_by_class_type(file, fpath)
    case file.class.to_s
    when GoogleDrive::File.to_s
      # puts file.mime_type
      download_by_mime_type(file, fpath)
    when GoogleDrive::Spreadsheet.to_s
      file.export_as_file("#{fpath}.xlsx")
      # when GoogleDrive::Collection.to_s
      # binding.pry
    else
      # binding.pry
      puts "[WARNING GDrive file] unknown: #{file.class}. Skipping..."
    end
  end

  def download_by_mime_type(file, fpath)
    case file.mime_type
    when 'application/vnd.google-apps.document'
      file.export_as_file("#{fpath}.docx")
    when 'application/vnd.google-apps.presentation'
      file.export_as_file("#{fpath}.pptx")
    else
      # binding.pry
      content = file.download_to_string
      File.write(fpath, content)
    end
  end

  def select_drive_links(links)
    links.select { |lnk| lnk.match?('drive.google.com/') || (lnk.match?('docs.google.com') && !lnk.match?('/forms')) }
  end

  def log_folder_messages(folder, path, url)
    puts "[INFO] Visiting #{url}"
    puts "[INFO] Saving to #{path}"
    puts "[INFO] Entering GDrive folder #{folder.title}"
  end

  def file_not_present?(fpath)
    !File.exist?(fpath) && SUPPORTED_EXTENSIONS.all? { |ext| !File.exist?("#{fpath}#{ext}") }
  end

  def folder_id_from_url(url)
    url.split('/').last.split('?').first
  end
end

# Google Drive download logic
class GoogleDriveDownloader
  include GoogleDriveHelper

  attr_reader :scraper

  def initialize(scraper)
    @gdrive_visited = []
    @scraper = scraper
    init_session
  end

  def process_drive_links(full_resource_name, links)
    drive_links = select_drive_links(links)
    drive_links.each do |link|
      # add https if not present
      link = link.split('https://').last.prepend('https://')

      process_drive_link(link, full_resource_name)
      @gdrive_visited << link
    rescue StandardError => e
      puts '[ERROR GDrive links]'
      puts e, link
    end
  end

  private

  def process_drive_link(link, resource_name)
    case link
    when %r{/drive/(u/.+/)?folders/}
      download_gdrive_folder(link, "drive/#{resource_name}/")
    when %r{/open}
      # discard everything after usp since that is used for sharing
      save_content_by_id(link, resource_name)
    when %r{docs.google.com/spreadsheets/d/e}
      save_spreadsheet(link, resource_name)
    else
      save_other_file(link, resource_name)
    end
  end

  def save_content_by_id(link, resource_name)
    id = link.split('id=').last.split('?usp').first
    file = @session.file_by_id(id)
    save_gdrive_file(file, "drive/#{resource_name}/")
  end

  def save_other_file(link, resource_name)
    file = @session.file_by_url(link)
    save_gdrive_file(file, "drive/#{resource_name}/")
  end

  def save_spreadsheet(link, resource_name)
    link = make_export_link(link)
    resp = URI.parse(link).open
    # binding.pry if resp.status[0] != "200"
    file_name = Addressable::URI.unescape resp.meta['content-disposition'].split('filename*=')[1].split("''")[1]

    scraper.save_file("drive/#{resource_name}/", file_name, content: resp.read)
    puts '[INFO] Sleeping...'
    sleep 5
  end

  def make_export_link(link)
    "#{link.split('/pubhtml').first}/pub?output=xlsx" unless link.end_with?('/pub?output=xlsx')
  end

  def save_gdrive_file(file, path)
    FileUtils.mkdir_p(path) unless File.exist?(path)

    # binding.pry
    fpath = "#{path}/#{file.title}"
    if file_not_present?(fpath)
      puts "[INFO] Attempting to create #{fpath}..."
      download_by_class_type(file, fpath)
    end
  rescue StandardError => e
    puts '[ERROR GDrive file]'
    # binding.pry
    puts e, file.id
  end

  def download_gdrive_folder(url, path)
    return if @gdrive_visited.member? url

    FileUtils.mkdir_p(path)
    folder = @session.folder_by_id(folder_id_from_url(url))
    log_folder_messages(folder, path, url)

    @gdrive_visited << url
    save_content(folder, path)
    puts "[INFO] Leaving GDrive folder #{folder.title}"
  rescue StandardError => e
    # binding.pry
    puts '[ERROR GDrive folder]'
    puts e, url
  end

  def save_content(folder, path)
    save_subfolders(path, folder.subfolders)
    # folder.documents.each do |doc|
    # binding.pry
    # puts doc
    # end
    save_files(folder, path, folder.subfolders)
  end

  def save_files(folder, path, subfolders)
    files = folder.files
    files.reject { |f| subfolders.map(&:id).member? f.id }.each do |file|
      fpath = "#{path}/#{folder.title}"
      save_gdrive_file(file, fpath)
      @gdrive_visited << file.human_url
    end
  end

  def save_subfolders(path, subfolders)
    subfolders.each { |sf| download_gdrive_folder(sf.human_url, "#{path}/#{sf.title}") }
  end

  def init_session
    @session = GoogleDrive::Session.from_config(
      'config.json',
      client_options: {
        open_timeout_sec: 30,
        read_timeout_sec: 30,
        send_timeout_sec: 30
      }
    )
  end
end

# The main scraper class
class Scraper
  FMI_DIR = 'fmi_files/'
  FMI_ROOT = 'https://fmi.unibuc.ro/'

  attr_reader :url_list, :new_urls

  def initialize(root: FMI_ROOT)
    @root = root.dup
    @visited = []
    @new_urls = []
    @google_drive_downloader = GoogleDriveDownloader.new(self)

    init_visited

    FileUtils.mkdir_p(FMI_DIR) unless File.exist?(FMI_DIR)
    FileUtils.mkdir_p('drive') unless File.exist?('drive')
  end

  def save_file(dir, file, url: nil, content: nil)
    file_name = "#{dir}#{file}".tr("\n", '')
    return unless blank_file?(file_name)

    puts "[INFO] Attempting to create #{file_name}..."
    url&.tr!("\n", '')
    content ||= URI.parse(url).open.read
    File.write(file_name, content)
  end

  def recursive_lookup(url: @root)
    url.tr!("\n", '')
    return if @visited.member? url

    puts "[INFO] Visiting #{url}"
    @visited << url

    # @new_urls << url if !@url_list.member?(url) && !@new_urls.member?(url)

    links = find_links_and_save_content(url)

    remaining = select_remaining_links(links)
    remaining.each { |link| recursive_lookup(url: link) }
  rescue StandardError => e
    puts '[ERROR]'
    puts e, url
  end

  private

  def find_links_and_save_content(url)
    full_resource_name, resource_name = resource_names(url)

    save_file(FMI_DIR, "#{full_resource_name}.html", url: url) if url_is_file?(resource_name, url)

    content = URI.parse(url).open
    links = parse_links(content, url)

    @new_urls += links
    @new_urls.uniq!

    @google_drive_downloader.process_drive_links(full_resource_name, links)

    links
  end

  def resource_names(url)
    url_parts = url.split('/')
    resource_name = url_parts.last
    full_resource_name = url_parts[2..-1].join('_')

    [full_resource_name, resource_name]
  end

  def url_is_file?(resource_name, url)
    url != '' && !url.match?('%23') && resource_name && !short_url?(url)
  end

  def select_remaining_links(links)
    links.select! { |link| link.match?(FMI_ROOT) || link.start_with?('/') || short_url?(link) }
    links.reject!(&method(:fragment?))
    links.map! do |link|
      link.prepend(@root) if link.start_with?('/')
      link
    end
    # binding.pry
    links - @visited
  end

  def fragment?(link)
    link.match?('%23') || link.match?('#')
  end

  def parse_links(content, url)
    if short_url?(url)
      [content.base_uri.to_s]
    else
      doc = Nokogiri::HTML(content)

      doc.xpath('//a').map { |x| x.attributes['href']&.value }.compact.uniq
    end
  end

  def blank_file?(file_name)
    !File.exist?(file_name) || File.size(file_name).zero?
  end

  def short_url?(url)
    url.match?('tinyurl') || url.match?('bit.ly')
  end

  def init_visited
    @url_list = if File.exist?('url_list.txt')
                  File.readlines('url_list.txt').map(&:strip)
                else
                  FileUtils.touch('url_list.txt')

                  []
                end
  end
end

start_time = Time.now
puts "[INFO] Started at #{start_time}"

scraper = Scraper.new
scraper.recursive_lookup

File.open('url_list.txt', 'a') do |file|
  (scraper.new_urls.uniq - scraper.url_list.uniq).each { |url| file.puts url }
end

end_time = Time.now
puts "[INFO] Finished at #{end_time}. Total time: #{end_time - start_time} seconds"
